

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import mountainhuts.*;

public class TestClass {
	
	Region r;
	
	@Before
	public void setUp() {
		r = Region.fromFile("Piemonte", "mountain_huts.csv");
	}
	
	@Test
	public void testR1() {
		assertEquals("Piemonte", r.getName());
		String[] ranges = {
		                 "0-1000",
		                 "1000-2000",
		                 "2000-3000"
		                 };
		r.setAltitudeRanges(ranges);
		assertEquals(r.getAltitudeRange(500), "0-1000");
		assertEquals(r.getAltitudeRange(3500), AltitudeRange.DEFAULT.toString());
	}
	
	@Test
	public void testR2Municipalities() {
		int municipalityStartNumber = r.getMunicipalities().size();
		r.createOrGetMunicipality("NonExisting", "TORINO", 1564);
		assertEquals(r.getMunicipalities().size(), municipalityStartNumber + 1);
		Municipality m = r.createOrGetMunicipality("BIELLA", "Whatever", 2);
		assertTrue(r.getMunicipalities().size() == municipalityStartNumber + 1);
		assertEquals(m.getName(), "BIELLA");
		assertEquals(m.getProvince(), "BIELLA");
		assertTrue(m.getAltitude() == 420);
	}
	
	@Test
	public void testR2MountainHuts() {
		int hutsStartNumber = r.getMountainHuts().size();
		r.createOrGetMountainHut("Non existing", "Rifugio alpino", 1, r.createOrGetMunicipality("BIELLA", null, null));
		assertTrue(r.getMountainHuts().size() == hutsStartNumber + 1);
		MountainHut m = r.createOrGetMountainHut("HINDERBALMO", "Whatever", 2, r.createOrGetMunicipality("BIELLA", null, null));
		assertTrue(r.getMountainHuts().size() == hutsStartNumber + 1);
		assertEquals(m.getName(), "HINDERBALMO");
		assertTrue(m.getAltitude().isEmpty());
		assertTrue(m.getBedsNumber() == 0);
	}
	
	@Test
	public void testR2Altitudes() {
		Municipality m = r.createOrGetMunicipality("BIELLA", "Whatever", 2);
		assertTrue(m.getAltitude() == 420);
		MountainHut mh1 = r.createOrGetMountainHut("HINDERBALMO", "Whatever", 2, r.createOrGetMunicipality("BIELLA", null, null));
		assertTrue(mh1.getAltitude().isEmpty());
		MountainHut mh2 = r.createOrGetMountainHut("CAPANNA FELICE GIORDANO", "Whatever", 2, r.createOrGetMunicipality("BIELLA", null, null));
		assertTrue(mh2.getAltitude().get() == 4167);
	}
	
	@Test
	public void testR2Getters() {
		Collection<Municipality> municipalities = r.getMunicipalities();
		Collection<MountainHut> mountainhuts = r.getMountainHuts();
		assertFalse(municipalities.isEmpty());
		assertFalse(mountainhuts.isEmpty());
	}
}